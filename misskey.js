function get_png_files() {
    let files = [];

    for (const file of Deno.readDirSync(".")) {
        if (file.isFile && /.png$/.test(file.name)) { files.push(file.name) }
    }

    return files;
}

function misskey_custom_emoji_manifest(png_files, category) {
    return {
        generatedAt: (new Date()).toISOString(),
        emojis: png_files.map((filename) => {
            return {
                fileName: filename,
                downloaded: true,
                emoji: {
                    name: filename.split(".")[0],
                    category: category,
                    aliases: [""],
                    isSensitive: false,
                    localOnly: false
                }
            }
        })
    }
}

function main() {
    const manifest = misskey_custom_emoji_manifest(get_png_files(), "arkn-op");
    console.log(JSON.stringify(manifest, null, 4));
}

main();
